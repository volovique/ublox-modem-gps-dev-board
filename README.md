# README #

UBLOX SARA-U270 + MAX8C Development Board

### What is this repository for? ###

* To test Ublox Sara270 modem together with Max 8C GPS
* Version: V1.0
* **First board revision can contains mistakes on schematic and PCB**

### How do I get set up? ###

* Connect micro USB only
* To configure net state LED diode it is necessary to issue AT command AT+UGPIOC=16,2
* To bring up MAX8C GPS it is necessary to issue AT command (for instance AT+UGPS=1,0,1)
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* GPS activation through SARAU270 modem
* 1. Send AT+UGPS=1,0,1 to activate GPS and choose GPS and no aiding
* 2. Send AT+UGGSV=1 for activation and AT+UGGSV? to read GNSS satellites in view
* 3. Sebd AT+UGGSA=1 and that AT+UGGSA? to get satellite information
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact